<?php

class ProductsController extends BaseController
{
	public function getSalesAll()
	{
		$productID = $this->getAllID();
		return $this->returnProductsByID($productID, 1);
	}

	public function getExternalAll()
	{
		$productID = $this->getAllID();
		return $this->returnProductsByID($productID, 2);
	}

	public function postSalesSearch($searchMe)
	{
		$allData = Input::json()->all();
		$allData = explode(',',$allData[0]['param']);
		$productID = $this->commonSearch($searchMe, 1, $allData);
		return $this->returnProductsByID($productID, 1);
	}

	public function postExternalSearch($searchMe)
	{
		$allData = Input::json()->all();
		$allData = explode(',',$allData[0]['param']);
		$productID = $this->commonSearch($searchMe, 2, $allData);
		return $this->returnProductsByID($productID, 2);
	}

	public function getSalesSearchID($searchMe)
	{
		return $this->returnProductsByID($searchMe, 1);
	}

	public function getExternalSearchID($searchMe)
	{
		return $this->returnProductsByID($searchMe, 2);
	}

	private function commonSearch($searchMe, $type, $param)
	{
		$resultByName='';		$resultBySummary='';		$resultByRoles='';		$resultBySkillsets='';		$resultByTags='';		$resultByTypes ='';
		if(in_array('name', $param))
		{
			$resultByName = $this->searchByName($searchMe);
		}
		if(in_array('summary', $param))
		{
			$resultBySummary = $this->searchBySummary($searchMe);
		}
		if(in_array('roles', $param))
		{
			$resultByRoles = $this->searchByRole($searchMe);
		}
		if(in_array('skillSets', $param))
		{
			$resultBySkillsets = $this->searchBySkillset($searchMe);
		}
		if(in_array('tags', $param))
		{
			$resultByTags = $this->searchByTags($searchMe);
		}
		if(in_array('types', $param))
		{
			$resultByTypes = $this->searchByTypes($searchMe);
		}
		$productID = $resultByName.','.$resultBySummary.','.$resultByRoles.','.$resultBySkillsets.','.$resultByTags.','.$resultByTypes;
		if($type==1 && in_array('company', $param))
		{
			$resultByCompany = $this->searchByCompany($searchMe);
			$productID = $productID .','. $resultByCompany;
		}
		$productID = implode(',',array_values(array_filter((array_unique(explode(',', $productID))))));
		return $productID;
	}

	private function getAllID()		//Get all Product ID's
	{
		$result = Products::all()->toArray();
		$result = array_map(function($item){return $item['prd_id'];}, $result);
		$result = implode(',',$result);
		return $result;
	}
	private function searchByName($searchMe)
	{
		$resultByName = Products::where('prd_name', 'LIKE', '%'.$searchMe.'%')->select(array('prd_id'))->get()->toArray();
		$resultByName = array_map(function($item){return $item['prd_id'];}, $resultByName);
		$resultByName = implode(',',$resultByName);
		return $resultByName;
	}
	private function searchBySummary($searchMe)
	{
		$resultBySummary = Products::where('prd_summary', 'LIKE', '%'.$searchMe.'%')->select(array('prd_id'))->get()->toArray();
		$resultBySummary = array_map(function($item){return $item['prd_id'];}, $resultBySummary);
		$resultBySummary = implode(',',$resultBySummary);
		return $resultBySummary;
	}
	private function searchByRole($searchMe)
	{
		$allProducts = $this->getAllID();
		$allProducts = explode(',',$allProducts);
		$productID='';
		foreach($allProducts as &$results)
		{
			$resultByRole = Products::find($results)->roles()->where('roles.rol_role', 'LIKE', '%'.$searchMe.'%')->get()->toArray();
			foreach($resultByRole as $result)
			{
				$productID = $productID . ','.$result['pivot']['pdr_prd_id'];
			}
		}
		$productID = substr($productID, 1);
		return $productID;
	}
	private function searchBySkillset($searchMe)
	{
		$allProducts = $this->getAllID();
		$allProducts = explode(',',$allProducts);
		$productID='';
		foreach($allProducts as &$results)
		{
			$resultBySkillset = Products::find($results)->skillsets()->where('skill_sets.sks_skill_set', 'LIKE', '%'.$searchMe.'%')->get()->toArray();
			foreach($resultBySkillset as $result)
			{
				$productID = $productID . ','.$result['pivot']['pds_prd_id'];
			}
		}
		$productID = substr($productID, 1);
		return $productID;
	}
	private function searchByTags($searchMe)
	{
		$allProducts = $this->getAllID();
		$allProducts = explode(',',$allProducts);
		$productID='';
		foreach($allProducts as &$results)
		{
			$resultByTags = Products::find($results)->tags()->where('tags.tag_tag', 'LIKE', '%'.$searchMe.'%')->get()->toArray();
			foreach($resultByTags as $result)
			{
				$productID = $productID . ','.$result['pivot']['pdt_prd_id'];
			}
		}
		$productID = substr($productID, 1);
		return $productID;
	}
	private function searchByCompany($searchMe)
	{
		$allProducts = $this->getAllID();
		$allProducts = explode(',',$allProducts);
		$productID='';
		foreach($allProducts as &$results)
		{
			$resultByCompany = Products::find($results)->company()->where('company.cpy_name', 'LIKE', '%'.$searchMe.'%')->get()->toArray();
			foreach($resultByCompany as $result)
			{
				$productID = $productID . ','.$result['pivot']['pdc_prd_id'];
			}
		}
		$productID = substr($productID, 1);
		return $productID;
	}
	private function searchByTypes($searchMe)
	{
		$allProducts = $this->getAllID();
		$allProducts = explode(',',$allProducts);
		$productID='';
		foreach($allProducts as &$results)
		{
			$resultByTypes = Products::find($results)->types()->where('types.typ_type', 'LIKE', '%'.$searchMe.'%')->select(array('typ_id'))->get()->toArray();
			foreach($resultByTypes as $result)
			{
				$tempResult = Types::find($result['typ_id'])->products()->select(array('prd_id'))->get()->toArray();
				foreach($tempResult as $temp)
				{
					$productID = $productID . ','.$temp['prd_id'];
				}
			}
		}
		$productID = substr($productID, 1);
		return $productID;
	}

	private function getCommonData(&$results)
	{
		$resultByType = Products::find($results['prd_id'])->company()->select(array('company.cpy_name'))->get()->toArray();
		$resultByType = array_map(function($item){return $item['cpy_id'];}, $resultByType);
		$resultByType = implode(',',$resultByType);
		$results['prd_companyId'] = $resultByType;

		$resultByType = Products::find($results['prd_id'])->types()->select(array('types.typ_type'))->get()->toArray();
		$resultByType = array_map(function($item){return $item['typ_type'];}, $resultByType);
		$resultByType = implode(',',$resultByType);
		$results['prd_type'] = $resultByType;

		$resultByType = Products::find($results['prd_id'])->roles()->select(array('roles.rol_role'))->get()->toArray();
		$resultByType = array_map(function($item){return $item['rol_role'];}, $resultByType);
		$resultByType = implode(',',$resultByType);
		$results['prd_roles'] = $resultByType;

		$resultByType = Products::find($results['prd_id'])->skillsets()->select(array('skill_sets.sks_skill_set'))->get()->toArray();
		$resultByType = array_map(function($item){return $item['sks_skill_set'];}, $resultByType);
		$resultByType = implode(',',$resultByType);
		$results['prd_skillSets'] = $resultByType;

		$resultByType = Products::find($results['prd_id'])->tags()->select(array('tags.tag_tag'))->get()->toArray();
		$resultByType = array_map(function($item){return $item['tag_tag'];}, $resultByType);
		$resultByType = implode(',',$resultByType);
		$results['prd_tags'] = $resultByType;

		$resultbyTestimonials = Products::find($results['prd_id'])->testimonials()->get()->toArray();
		$testimonials = array();
		foreach($resultbyTestimonials as $individualResult)
		{
			$temp = array();
			$temp['tml_testimonial'] = $individualResult['tml_testimonial'];
			$company = Testimonials::find($individualResult['tml_id'])->company()->get()->toArray();
			$temp['tml_cpy_name'] = $company[0]['cpy_name'];
			array_push($testimonials, $temp);
		}
		$results['prd_testimonials'] = $testimonials;
	}

	private function getSalesData(&$results)
	{
		unset($results['prd_typ_id']);
		$res = Products::find($results['prd_id'])->lastClient()->select(array('cli_fname', 'cli_lname', 'cli_contact_number', 'cli_email', 'cli_cpy_id'))->get()->toArray();
		$res = $res[0];
		$res1 = Company::where('cpy_id','=', $res['cli_cpy_id'])->select(array('cpy_name'))->get()->toArray();
		$res['cpy_name'] = $res1[0]['cpy_name'];
		unset($res['cli_cpy_id']);
		$results['prd_last_client'] = $res;

		$res = Products::find($results['prd_id'])->developmentTeam()->get()->toArray();
		foreach($res as &$temp)
		{
			unset($temp['pivot'], $temp['emp_id'], $temp['emp_dob'], $temp['emp_add'], $temp['emp_rol_id']);
		}
		$results['prd_development_team'] = $res;

		$res = Products::find($results['prd_id'])->lastAccountManager()->select(array('emp_fname', 'emp_lname', 'emp_email'))->get()->toArray();
		$res = $res[0];
		$results['prd_last_ac_man'] = $res;
	}

	private function getExternalData(&$results)
	{
		unset($results['prd_price'], $results['prd_developed_date'], $results['prd_typ_id'], $results['prd_last_ac_man'], $results['prd_last_client']);
	}

	private function returnProductsByID($productID, $user)
	{
		$productID = array_values(array_filter((array_unique(explode(',', $productID)))));
		if(empty($productID))
		{
			return "NO RESULTS";
		}
		$resultByID = Products::whereIn('prd_id', $productID)->get()->toArray();
		foreach($resultByID as &$results)
		{
			$this->getCommonData($results);
			if($user == 1)
			{
				$this->getSalesData($results);
			}
			else
			{
				$this->getExternalData($results);
			}
		}
		if(empty($resultByID))
		{
			return "NO RESULTS";
		}
		return json_encode($resultByID);
	}
}